# -*- coding: utf-8 -*-
"""
Created on Mon Feb 22 10:45:40 2016

@author: kkalem
"""
import numpy as np
import geometry as geom
import matplotlib.pyplot as plt
import polySearch as ps
import matlabHandler as mh
import munkres as hun
import views as vw


class formation:
    d = 15
    raw = np.array([])
    real = np.array([])
    velocity = np.array([0,0])
    pos = np.array([0,0])
    pos_trace = []
    view = None
    cost_func = geom.euclidDistance

    def __init__(self,raw,d = 15, velocity = [0,0], pos = [0,0], view = vw.TracingView(), cost_func = geom.euclidDistance):
        self.raw = np.array(raw)
        self.d = d
        self.pos = np.array(pos)
        self.real = self.pos + self.raw * d
        self.velocity = np.array(velocity)
        self.view = view
        self.cost_func = cost_func
        self.m = hun.Munkres()


    def __str__(self):
        return 'formation;' + str(self.real.tolist()) + ' d;' + str(self.d)

    def moveTo(self,newpos):
        self.pos_trace.append(self.pos)
        self.pos = np.array(newpos)
        self.real = self.pos + self.raw * self.d
        return self

    def tick(self):
        self.pos_trace.append(self.pos)
#        if isinstance(self.view,vw.TracingView):
#            self.view.setPosition(self.real.tolist())
        self.view.syncSignal(0)
        self.real = self.real + self.velocity*self.view.getTimeStep()

    def draw(self):
        plt.scatter(self.real[:,0],self.real[:,1],alpha = 0.5, color = 'g', marker = 'x')
        return self

    def compute(self,model_positions):
        cost_matrix = np.zeros([len(model_positions),len(self.raw)])
        for row in range(len(model_positions)): # for each model
            for col in range(len(self.raw)): # to each point
                cost_matrix[row,col] = self.cost_func(model_positions[row],self.real[col]) #cost i,j = model i to point j
        indices = self.m.compute(cost_matrix)
        return indices


if __name__ == '__main__':
    view = vw.TracingView();
    f = formation([[0,0],[1,0],[2,0],[3,0],[0,1],[1,1],[2,1],[3,1],[1,2],[2,2]],d=50, view = view, velocity = [0,5])
    f.draw()

    s,g,x,y,edges,button,customer = mh.generateOBJstr('assn2map1.mat') #assn2map1 or map2
    #mh.draw2Dpolygons(x,y,edges,False)
    model_positions = np.array(s.tolist()+g.tolist())
    plt.scatter(model_positions[:,0],model_positions[:,1],marker = 'x')

    inds = f.compute(model_positions)

    for index in inds:
        plt.plot([model_positions[index[0],0],f.real[index[1],0]],[model_positions[index[0],1],f.real[index[1],1]])
