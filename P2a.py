# -*- coding: utf-8 -*-
"""
Created on Sun Feb 28 12:37:00 2016

@author: kkalem

Solve T2 above, with the same data as P1. The formation should move "north" with velocity 5.
"""
import matlabHandler as mh
import views as vw
import formation as form
import dynamic_point as dp
import dynamic_car as dc
import geometry as geom
import vrep
import numpy as np
import models as mdl

import matplotlib.pyplot as plt

s,g,x,y,edges,button,customer = mh.generateOBJstr('final5.mat') #assn2map1 or map2

starting = s.tolist()

doInVrep=False
scale=0.1
baseName='Cuboid'

if doInVrep:
    cid = vw.startVrep()
    err, handle = vrep.simxGetObjectHandle(cid, baseName, vrep.simx_opmode_oneshot_wait)
    objHandle=[handle]
    for st in starting:
        err, newFloorHandle = vrep.simxCopyPasteObjects(cid, [objHandle[0]] , vrep.simx_opmode_oneshot_wait)
        vrep.simxSetObjectPosition(cid, newFloorHandle[0], -1, (np.array(st)*scale).tolist(), vrep.simx_opmode_oneshot)
        objHandle.extend(newFloorHandle)
    view = vw.vrep_view(cid,'',scale)
else:
    view = vw.TracingView()
fx = 0.
fy = 0.
for i in range(len(starting)):
    fx += starting[i][0]
    fy += starting[i][1]
fx = fx / len(starting)
fy = fy / len(starting)


f = form.formation(g.tolist() ,d=1, view = view, velocity = [5,0], pos = [0.,0.])

ffx =0.
ffy = 0.
for p in f.real:
    ffx += p[0]
    ffy += p[1]
ffx = ffx/len(f.real)
ffy = ffy/len(f.real)

dx = ffx - fx
dy = ffy - fy

f.moveTo([f.pos[0]-dx,f.pos[1]-dy])

agents = []

i=0
for pos in starting:
    if doInVrep:
        view = vw.vrep_view(cid,objHandle[i+1],scale)
    else:
        view = vw.TracingView(init_pos = geom.in3d(pos), cont = False)
    agent = dp.DynamicPoint(view, 0, X = pos, size = 0.1)
    agent.view.reinit()
    agents.append(agent)
    i+=1

if not doInVrep:
    for agent in agents:
        r = agent.size
        c1 = plt.Circle(agent.X,r, fill = False)
        fig = plt.gcf()
        fig.gca().add_artist(c1)

plt.pause(10)
plt.axis('equal')
for time in range(10000):
#    plt.clf()    
    if time%10000==0:
        modelposs = [agents[k].X for k in range(len(agents))]
        indices = f.compute(modelposs)
    done = ''
    for j in range(len(agents)):
        agents[j].setTarget(f.real[indices[j][1]])
        done += agents[j].tick()
    if time%5==0:
        agents[0].view.syncSignal()
#        plt.scatter(agents[j].X[0],agents[j].X[1])
    f.tick()
    if done == 'done'*len(agents):
        print time
        break
    if time%100==0 and not doInVrep:
#        plt.pause(0.001)
        f.draw()

for agent in agents:
    vw.drawPath(agent.view.pos_trace)
    plt.scatter(agent.X[0], agent.X[1])
#f.draw()