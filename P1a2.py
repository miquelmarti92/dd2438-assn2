# -*- coding: utf-8 -*-
"""
Created on Sun Mar 06 19:23:48 2016

@author: kkalem
"""
import matlabHandler as mh
import views as vw
import formation as form
import dynamic_car as dc
import geometry as geom

import matplotlib.pyplot as plt

s,g,x,y,edges,button,customer = mh.generateOBJstr('assn2map1.mat') #assn2map1 or map2

starting = s.tolist() + g.tolist()
fx = 0.
fy = 0.
for i in range(len(starting)):
    fx += starting[i][0]
    fy += starting[i][1]
fx = fx / len(starting)
fy = fy / len(starting)

view = vw.TracingView();
f = form.formation([[0,0],[1,0],[2,0],[3,0],[0,1],[1,1],[2,1],[3,1],[1,2],[2,2]],d=50, view = view, pos = [fx-100,fy-70])

agents = []

for pos in starting:
    view = vw.TracingView(init_pos = geom.in3d(pos), cont = False)
    agent = dc.DynamicCar(view, 0, X = pos, size = 1.)
    agent.view.reinit()
    agents.append(agent)

modelposs = [agents[k].X for k in range(len(agents))]
indices = f.compute(modelposs)
for j in range(len(agents)):
    agents[j].addTarget(f.real[indices[j][1]])

for time in range(900):
    for agent in agents:
        agent.tick()
#    f.tick()

for agent in agents:
    vw.drawPath(agent.view.pos_trace)
    plt.scatter(agent.X[0], agent.X[1])
f.draw()