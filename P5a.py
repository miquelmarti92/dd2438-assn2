# -*- coding: utf-8 -*-
"""
Created on Wed Mar 02 15:48:45 2016

@author: kkalem

darpa mini challenge
1) plan route for agent
2) send agent on its marry way
3) dont crash into shit
"""

import matplotlib.pyplot as plt
#import numpy as np

import views
import matlabHandler as mh
import dynamic_point as dp
import polySearch as ps

#plt.figure(3)
plt.axis('equal')
s,g,x,y,edges,button,customer = mh.generateOBJstr('final4.mat') #assn2map1 or map2
mh.draw2Dpolygons(x,y,edges,False)
polygons = ps.polysToVerts(x,y,button)

poly_walls = []

for edge in edges:
    wall = [[x[int(edge[0])],y[int(edge[0])]],[x[int(edge[1])],y[int(edge[1])]]]
    poly_walls.append(wall)

agents = []
wps = [[] for i in range(len(s))]
#wps[1] = [[148.,63.],[134.,108.]]
#wps[2] = [[151.,53.]]

for i in range(len(s)):
    si = s[i]
    gi = g[i]
    wp = wps[i]
    agent = dp.DynamicPoint(views.TracingView(init_pos = si, cont=False), X = si, ID = i, size = 1.)
    agent.addTarget(wp,multi=True)
    agent.addTarget(gi)
    for wall in poly_walls: agent.addWall(wall)
    agents.append(agent)

for agent in agents:
    r = agent.size
    c1 = plt.Circle(agent.X,r, fill = False)
    fig = plt.gcf()
    fig.gca().add_artist(c1)
    for otherAgent in agents:
            if agent.ID != otherAgent.ID:
                agent.addAvoid(otherAgent)

plt.scatter(s[:,0],s[:,1], marker = '^')
plt.scatter(g[:,0],g[:,1], marker = 'v')
for time in range(15000):
    results = ''
    for agent in agents:
        result = agent.tick()
        results += result
#        plt.plot([search_agent.X[0],search_agent.X[0]+search_agent.A[0]],[search_agent.X[1],search_agent.X[1]+search_agent.A[1]], color = 'b', alpha = 0.1)
#        plt.plot([search_agent.X[0],search_agent.X[0]+search_agent.V[0]],[search_agent.X[1],search_agent.X[1]+search_agent.V[1]], color = 'g', alpha = 0.1)
    if time%3 == 0:
        plt.pause(0.0001)
    if results == 'done'*len(s):
        print 'COMPLETED IN', time*agents[0].view.getTimeStep(), 'SECONDS'
        break

#for a in range(24):
#    small_trace = []
#    for i in range(len(agents[a].view.pos_trace)):
#        if i%15==0:
#            small_trace.append(agents[a].view.pos_trace[i])
#    views.drawPath(small_trace)

#for agent in agents: views.drawPath(agent.view.pos_trace)
