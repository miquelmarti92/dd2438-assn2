# -*- coding: utf-8 -*-
"""
Created on Thu Mar 03 22:07:28 2016

@author: kkalem

"""

import numpy as np
import matplotlib.pyplot as plt

import views
import geometry as geom

class DynamicCar():
    def __init__(self,view, ID, X = [0.,0.], r = 0., v = 0., max_v = 10., max_a = 1., max_phi = np.pi/4, l = 10., size = 1.):
        self.view = view #view object
        self.ID = ID #ID of the car
        self.sim_time = 0 #time in ticks
        self.dt = self.view.getTimeStep()

        self.size = size #radius of collison circle

        self.X = np.array(X) #position of the car
        self.V = np.array([0.,0.])
        self.r = r #rotation from x axis
        self.v = v #speed, not velocity
        self.a = 0. #accel
        self.phi = 0. #current front wheel rotation
        self.l = l #length of the car
        self.max_v = max_v #max speed
        self.max_a = max_a #max accel
        self.max_phi = max_phi #max front wheel turning angle
        self.target = self.X #current target to reach

        self.target_list = [] #list of targets to reach
        self.avoid_list = [] #list of other agents to avoid
        self.wall_list = [] #list of walls as 2 point vectors

        self.__angle_thresh = 0.01 #angle between self and target, if larger than this, start turning
        self.phi_target = 0. #needed wheel angle to reach target
        self.a_target = 0. #needed accel to reach target
        self.v_target = 0. #desired speed at target
        self.target_vs = [] #desired speeds to reach and stop at the target

        self.turn_center1, self.turn_center2, self.turn_rad  = self.__generate_turning_circles()

        self.__stop_at_last_target = True
        self.target_range = self.size
        self.braking_factor = 5.

        self.__vel_obs_time = 6. #time in ticks for look-ahead for other agents
        self.velocity_obstacle = self.__generate_vel_obs(self.__vel_obs_time) #a cut-off cone
        self.bounding_box = self.__generate_bounding_box() #a box around the disc
        self.__detection_obstacle = self.__generate_detection_obs(self.__vel_obs_time) #a possibly different cone that is at most brake-dist long
        self.__wall_time = 15. # time in ticks for look-ahead for walls

        self._we_are_done_here = False #cheaty stopping

    def tick(self):
        if self._we_are_done_here:
            self.v = 0.
            self.V = np.array([0.,0.])
            self.a = 0.
            #update boxes
            self.velocity_obstacle = self.__generate_vel_obs(self.__vel_obs_time)
            self.bounding_box = self.__generate_bounding_box()
            self.__detection_obstacle = self.__generate_detection_obs(self.__vel_obs_time)
            self.turn_center1, self.turn_center2, self.turn_rad  = self.__generate_turning_circles()
            return 'done'
        #update boxes
        self.velocity_obstacle = self.__generate_vel_obs(self.__vel_obs_time)
        self.bounding_box = self.__generate_bounding_box()
        self.__detection_obstacle = self.__generate_detection_obs(self.__vel_obs_time)
        self.turn_center1, self.turn_center2, self.turn_rad  = self.__generate_turning_circles()

        ### AVOID STUFF
        for B in self.avoid_list:
            intersections = []
            for segment in self.__detection_obstacle.tolist():
                other_obstacle = B.velocity_obstacle.tolist()
                other_obstacle.extend(B.bounding_box.tolist())
                for other_segment in other_obstacle:
                    intersections.append(geom.find_intersection(segment[0],segment[1],other_segment[0],other_segment[1]))

            mindist = 9999
            closest_sect = None
            for sect in intersections:
                if sect != 'collinear' and sect is not None:
                    dist = geom.ptToLineSegment(self.X.tolist(),B.X.tolist(),sect)
                    if dist < mindist:
                        closest_sect = sect
                        mindist = dist

            if closest_sect is not None:
                self.target_phi = -self.phi
                self._doPhysics()
                return 'avoid'


        ### TURN TO TARGET
        H = self.target - self.X
        target_angle_to_self = np.arctan2(H[1],H[0])
        angle_diff = self.r - target_angle_to_self
        if np.abs(angle_diff) > self.__angle_thresh:
            if abs(angle_diff)>np.pi:
                angle_diff=angle_diff-np.sign(angle_diff)*2*np.pi
            if angle_diff < 0:
                self.phi_target = self.max_phi
            else:
                self.phi_target = -self.max_phi
        else:
            self.phi_target = 0.

        brake_dist = (self.v**2) / (2 * self.max_a)
        brake_dist = brake_dist * 2
        dist_to_target = geom.euclidDistance(self.X,self.target)
        e1 = geom.euclidDistance(self.target.tolist(),self.turn_center1)
        e2 = geom.euclidDistance(self.target.tolist(),self.turn_center2)
        #find the closer turn center
        if e1>e2:
            turn_center = self.turn_center2
            e = e2
        else:
            turn_center = self.turn_center1
            e = e1

        d = max(0,np.sqrt(e**2-self.turn_rad**2)) #distance from target to the tangent point of the turning circle

        r1 =  turn_center - self.X

        r2e_angle = np.arctan2(d,self.turn_rad)

        r2 = turn_center + e * np.array([np.cos(r2e_angle),np.sin(r2e_angle)])
        r2 = geom.normalize(r2) * self.turn_rad

        angle_r1_r2 = np.arccos(np.dot(r1,r2)/(geom.norm(r1)*geom.norm(r2)))
        c = (2*np.pi*self.turn_rad)*(angle_r1_r2/np.pi)

        path_to_target = c+d

        if len(self.target_list) == 0: #last target
            if dist_to_target <= self.target_range: #reached?
                if self.__stop_at_last_target: #wanna stop
                    if self.v > 1:
                        self.a_target = -self.max_a
                        self._we_are_done_here = True
                    else:
                        self.a_target = -self.v
                        self._we_are_done_here = True
                else: #wanna drift
                    self.a_target = 0.
            elif path_to_target-brake_dist <= 0: #did not reach but in brake distance
                if self.__stop_at_last_target: #wanna stop
                    self.a_target = -self.max_a
                else: #wanna drift
                    self.a_target = 0.
            else: #did not reach, not in braking distance, accelerate
                self.a_target = self.max_a
        else: #not last target
            self.a_target = self.max_a #no reason to slow down
            if dist_to_target <= self.target_range: #reached, change target
                self.target = self.target_list[0] #this is a queue
                self.target_list = self.target_list[1:] #remove the first element of the list


        self._doPhysics()
        return 'cont'

    def __generate_turning_circles(self):
        rad = self.l / np.tan(self.max_phi)
        heading = geom.normalize(self.V)
        perp1 = geom.perp_vec(heading)
        perp2 = -1*perp1
        c1 = self.X + (perp1 * rad)
        c2 = self.X + (perp2 * rad)
        return c1,c2,rad

    def __generate_bounding_box(self):
        #no 'heading', make a bounding box
        p1 = self.X + [self.size , self.size] #forward, up
        p2 = self.X + [self.size , -self.size] #forward, down
        p4 = self.X + [-self.size , self.size] #backward, down
        p3 = self.X + [-self.size , -self.size] #backward, up

        return np.array([[p1,p2],[p2,p3],[p3,p4],[p4,p1]])

    #generates a parallelogram that represents the most likely places this agent will be in look_ahead time
    def __generate_vel_obs(self,look_ahead):
        if geom.norm(self.V) == 0:
            #no 'heading', make a bounding box
            return self.__generate_bounding_box()
        else:
            #heading is velocity, make a rectangle
            norm_self_V = geom.normalize(self.V)
            perp_self_V = geom.normalize(geom.perp_vec(self.V))

            p4 = self.X + perp_self_V*self.size - self.size*norm_self_V #backward, down
            p3 = self.X - perp_self_V*self.size - self.size*norm_self_V #backward, up
            biggest_change = self._constrainV((self.max_a*perp_self_V) + self.V)
            p1base = self.X + norm_self_V*self.size + perp_self_V*self.size
            p2base = self.X + norm_self_V*self.size - perp_self_V*self.size
            forward = (self.V * look_ahead)
            p1 = p1base + forward + biggest_change #forward, up
            p2 = p2base + forward - biggest_change#forward, down

        return np.array([[p1,p2],[p2,p3],[p3,p4],[p4,p1]])

    #generates a parallelogram that this agent will use to avoid any other object.
    #same as vel. obs. except this is capped at brake distance.
    def __generate_detection_obs(self,look_ahead):
        if geom.norm(self.V) == 0:
            #no 'heading', make a bounding box
            return self.__generate_bounding_box()
        else:
            #heading is velocity, make a rectangle
            norm_self_V = geom.normalize(self.V)
            perp_self_V = geom.normalize(geom.perp_vec(self.V))

            p4 = self.X + perp_self_V*self.size - self.size*norm_self_V #backward, down
            p3 = self.X - perp_self_V*self.size - self.size*norm_self_V #backward, up
            biggest_change = self._constrainV((self.max_a*perp_self_V) + self.V)
            p1base = self.X + norm_self_V*self.size + perp_self_V*self.size
            p2base = self.X + norm_self_V*self.size - perp_self_V*self.size
            forward = (self.V * look_ahead)
            brake_dist = (geom.norm(self.V)**2) / (2* self.max_a)
            if geom.norm(forward) > brake_dist: #dont look further than your brake distance
                forward = geom.normalize(forward)*brake_dist
            p1 = p1base + forward + biggest_change #forward, up
            p2 = p2base + forward - biggest_change#forward, down

        return np.array([[p1,p2],[p2,p3],[p3,p4],[p4,p1]])

    def _doPhysics(self):
        ###temp!!
        self.phi = self.phi_target
        self.a = self.a_target


        if np.abs(self.a) > self.max_a:
            self.a = np.sign(self.a)*self.max_a
        self.v += min(self.max_v,self.a * self.dt)

        self.V = np.array([self.v*np.cos(self.r),self.v*np.sin(self.r)])
#        geom.plot_line([self.X,self.X+self.V], color = 'g', alpha = 0.1)

        rp = (np.tan(self.phi) * self.v / self.l)

        self.X += self.V * self.dt
        self.r += rp * self.dt

        #advance timer
        self.sim_time+=self.dt
        #update other stuff
        self.view.setPosition(self.X.tolist())
        self.view.setOrientation([0,0,self.r])
#        self.view.syncSignal(self.sim_time)

    def _constrainV(self,V):
        if geom.norm(V) > self.max_v:
            V = geom.normalize(V) * self.max_v
        return V

    def addTarget(self,T,multi=False):
        if multi:
            arraylist = [np.array(ti) for ti in T]
            self.target_list.extend(arraylist)
        else:
            self.target_list.append(np.array(T))

    def setTarget(self,T):
        self.target = np.array(T)
        self._we_are_done_here = False #cheaty stopping

    def addAvoid(self,other):
        self.avoid_list.append(other)

    def addWall(self,wall,multi=False):
        if multi:
            arraylist = [w for w in wall]
            self.wall_list.extend(arraylist)
        else:
            self.wall_list.append(wall)

    def followWaypoints(self,points):
        self.addTarget(points,multi=True)
        for time in range(10000):
            res = self.tick()
            if res == 'done': break
            if time%30 == 0: plt.pause(0.0001)
        return time

if __name__ == '__main__':
    plt.axis('equal')
    models = []

    s1 = [0.,0.]
    g1 = [40.,0.]
    g2 = [20.,50.]
    g3 = [0.,100.]
    plt.scatter(s1[0],s1[1])
    m1 = DynamicCar(views.TracingView(init_pos = s1, cont=True),ID=-1,X=s1)
    m1.addTarget(g1)
#    m1.addTarget(g2)
#    m1.addTarget(g3)
    models.append(m1)
    for g in m1.target_list:
        plt.scatter(g[0],g[1], marker = 'x')

    s2 = [40.,0.]
    g2 = [0.,0.]
    m2 = DynamicCar(views.TracingView(init_pos = s2, cont=True),ID=0,X=s2)
    m2.addTarget(g2)
    m2.addAvoid(m1)
    models.append(m2)
    m1.addAvoid(m2)

    for model in models:
            r = model.size
            c1 = plt.Circle(model.X,r, fill = False)
            fig = plt.gcf()
            fig.gca().add_artist(c1)

    for time in range(1500):
        for model in models:
            model.tick()
        if time%10 == 0:
            plt.pause(0.0001)

