# -*- coding: utf-8 -*-
"""
Created on Wed Jan 27 18:16:26 2016

@author: kkalem
"""

import scipy.io as sio
import numpy as np
import matplotlib.pyplot as plt
import vrep

######################################## POLYGONAL MAZE HANDLING ##########

#loads the given .mat file, returns the fields that are relevant
def getPolygons(polygObstMat = 'polygObst.mat'):
    p = sio.loadmat(polygObstMat)
    button = np.array([])
    x = np.array([])
    y = np.array([])
    customer = np.array([])
    try:
        button = p['button']
        x = p['x']
        y = p['y']
        customer = p['customerPos']
    except KeyError:
        pass
    start = p['startPos']
    goal = p['goalPos']

    return start, goal, button, x, y, customer

#returns a matrix of [n,2] that contains the indices of x and y that make an edge
def getEdges(button, x, y):
    firstEdge = 0;
    edges = np.zeros([len(x),2])

    for i in range(len(x)):
        edges[i,0] = i;
        if button[i] == 1:
            edges[i,1] = i+1
        elif button[i] == 3:
            edges[i,1] = firstEdge
            firstEdge = i + 1

    return edges

#converts the given 2D map into a 3D map.
def getLoHiVerts(x,y,height=5):
    loVerts = [[x[i][0],y[i][0],0] for i in range(len(x))]
    hiVerts = [[x[i][0],y[i][0],height] for i in range(len(x))]
    return loVerts, hiVerts


#converts the given edge matrix and low vertices into the edge matrix of the higher vertices.
def getEdgesOfHighLevel(loEdges,loVerts):
    highEdges = []
    for i in range(len(loEdges)):
        highEdges.append([loEdges[i][0]+len(loVerts),loEdges[i][1]+len(loVerts)])
    highEdges = np.array(highEdges)
    return highEdges

#puts all the vertices into the same list to convert into .obj
def addHighsToLowVertices(loVerts, hiVerts):
    verts = []
    for i in range(len(loVerts)):
        verts.append(loVerts[i])
    for i in range(len(hiVerts)):
        verts.append(hiVerts[i])
    return verts

#returns the array of edges that conenct the lower part to the higher part
def getVerticalEdges(loVerts):
    verticalEdges = []
    for i in range(len(loVerts)):
        verticalEdges.append([i,i+len(loVerts)])
    verticalEdges = np.array(verticalEdges)
    return verticalEdges

#returns the quads of verts in ccw order. starting from the bottom left
def generateFaces(loEdges, hiEdges):
    faces = []
    for i in range(len(loEdges)):
        face = [loEdges[i][0], loEdges[i][1], hiEdges[i][1], hiEdges[i][0]]
        faces.append(face)
    faces = np.array(faces)
    return faces.astype(int)

#returns the string representation of vertices for the wavefront object file
def generateVertexString(allVerts):
    vertStr = ""
    for v in allVerts:
        s = "v "
        for i in range(3):
            s = s+ "{0:04f}".format(v[i]) + " "
        vertStr = vertStr + s + "\n"
    return vertStr

#returns the string representation of faces for the wavefront object file
def generateFaceString(faces):
    strFaces = ""
    for f in faces:
        s = "f "
        for i in range(4):
            s = s+ str(f[i]+1) + " "
        strFaces = strFaces + s + "\n"
    return strFaces

#generates the required string representation of the 3D maze model for the wavefront obj file, saves it into
#a file and returns the string
def generateOBJstr(polygObstMat = 'polygObst.mat', output = 'output.obj'):
    start, goal, button, x, y, customer = getPolygons(polygObstMat)
    loEdges = getEdges(button, x, y)
    loVerts, hiVerts = getLoHiVerts(x,y)
    hiEdges = getEdgesOfHighLevel(loEdges, loVerts)
    allVerts = addHighsToLowVertices(loVerts,hiVerts)
    faces = generateFaces(loEdges,hiEdges)
    strVerts = generateVertexString(allVerts)
    strFaces = generateFaceString(faces)

    o = 'o maze \n' + strVerts + strFaces
    with open(output, 'w') as file_:
        file_.write(o)


    x,y,button = _fixxandyandbutton(x,y,button)
    return start.astype(float), goal.astype(float), x, y, loEdges, button, customer

def _fixxandyandbutton(x,y,b):
    xl = []
    yl = []
    bl = []
    for i in range(len(x)):
        xl.append(x[i][0])
        yl.append(y[i][0])
        bl.append(b[i][0])
    return xl, yl, bl



#draws the 2D map as a plot
def draw2Dpolygons(x,y,loEdges,show = True):
    for i in range(len(x)):
        plt.plot([x[loEdges[i,0].astype(int)], x[loEdges[i,1].astype(int)]],[y[loEdges[i,0].astype(int)], y[loEdges[i,1].astype(int)]], color='b', alpha=0.5)
    if show:
        plt.show()
######################################## DISCRETE MAZE HANDLING ##########


#loads the given .mat file, returns the fields that are relevant
def getDiscreteObstacleMap(discObstMat = 'discObst.mat'):
    discObst = sio.loadmat(discObstMat)
    discMap = discObst['A']
    goal = discObst['goalPos']-1
    start = discObst['startPos']-1
    return np.array(start).astype(int)[0], np.array(goal).astype(int)[0], np.array(discMap).astype(int)

#generates the given discrete obstacle map in the v-rep envrioment. The given handle will be used as obstacles
def generateDiscreteMaze(cid, wallHandle, discObstMat = 'discObst.mat'):
    spacing = 0.1
    height = 0.1
    s, g, m = getDiscreteObstacleMap(discObstMat)

    for i in range(len(m[0,:])):
        for j in range(len(m[:,0])):
            if m[i,j] == 1:
                newPos = [spacing*i, spacing*j, height*m[i,j]]
                err, newFloorHandle = vrep.simxCopyPasteObjects(cid, [wallHandle], vrep.simx_opmode_oneshot_wait)
                vrep.simxSetObjectPosition(cid, newFloorHandle[0], -1, newPos, vrep.simx_opmode_oneshot)
    return s, g, m


