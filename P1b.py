# -*- coding: utf-8 -*-
"""
Created on Fri Mar 04 16:56:38 2016

@author: Miquel
"""

# -*- coding: utf-8 -*-
"""
Created on Sun Feb 28 13:27:48 2016

@author: kkalem


P1a: Solve T1 above, with the given 4-4-2 formation, d=50,
and the 10 starting positions given by the 5 starting (*) and
5 ending (x) positions of polygObst above. The vehicle model is
 the dynamic point, with (Amax=1, Vmax=100).
"""

import matlabHandler as mh
import views as vw
import formation as form
import dynamic_point as dp
import geometry as geom
import numpy as np
import vrep

import matplotlib.pyplot as plt

s,g,x,y,edges,button,customer = mh.generateOBJstr('final5.mat') #assn2map1 or map2

R=2000
doInVrep=True
scale=0.1
baseName='port_sphere'

starting = s.tolist()
if doInVrep:
    cid = vw.startVrep()
    err, handle = vrep.simxGetObjectHandle(cid, baseName, vrep.simx_opmode_oneshot_wait)
    objHandle=[handle]
    for st in starting:
        err, newFloorHandle = vrep.simxCopyPasteObjects(cid, [objHandle[0]] , vrep.simx_opmode_oneshot_wait)
        vrep.simxSetObjectPosition(cid, newFloorHandle[0], -1, (np.array(st)*scale).tolist(), vrep.simx_opmode_oneshot)
        objHandle.extend(newFloorHandle)



agents = []
i=0
for pos in starting:
    if doInVrep:
        view = vw.vrep_view(cid,objHandle[i+1],scale)
    else:
        view = vw.TracingView(init_pos = geom.in3d(pos), cont = True)    
    agent = dp.DynamicPoint(view, i, X = pos, size = 0.)
    agent.view.reinit()
    agents.append(agent)
    i+=1




#BRING TOGETHER
while any(len(a.veh_in_range)<len(agents) for a in agents):
    for agent in agents:
        #Sense surroundings
        inrange=[a for a in agents if np.abs(np.linalg.norm(a.X-agent.X))<R]
        agent.addVehiclesRange(inrange)
        #Compute center of mass
        fx = 0.
        fy = 0.
        for v in agent.veh_in_range.values():
            fx += v.X[0]
            fy += v.X[1]
        fx = fx / len(agent.veh_in_range)
        fy = fy / len(agent.veh_in_range)
        #Set center of mass as target
        agent.setTarget([fx,fy])
    for agent in agents:
        #Tick
        agent.tick()
    agents[0].view.syncSignal()
    
fx = 0.
fy = 0.
for v in agents:
    fx += v.X[0]
    fy += v.X[1]
fx = fx / len(agents)
fy = fy / len(agents)

f = form.formation(g.tolist(),d=1, view = view, pos = [fx,fy])
modelposs = [agents[k].X for k in range(len(agents))]
indices = f.compute(modelposs)

for j in range(len(agents)):
    agents[j].setTarget(f.real[indices[j][1]])


##INTO FORMATION
for time in range(1500):
    modelposs = [agents[k].X for k in range(len(agents))]
    indices = f.compute(modelposs)
    for agent in agents:
        agent.setTarget(f.real[indices[agent.ID][1]])
        agent.tick()
    agents[0].view.syncSignal()
    
if doInVrep:
    vw.stopVrep(cid)
else:
    f.draw()
    for agent in agents:
        plt.scatter(agent.X[0], agent.X[1])

for a in agents:
    print a.sim_time