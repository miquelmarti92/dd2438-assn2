# -*- coding: utf-8 -*-
"""
Created on Tue Feb 02 21:10:02 2016

@author: kkalem
"""
import matlabHandler as mh
import numpy as np
import matplotlib.pyplot as plt
import copy
import models as md
import geometry as geom
import views as vw

__kin_pnt_vmax = 10
__dyn_pnt_vmax_a = 20
__dyn_pnt_amax_a = 40

__dyn_pnt_vmax_b = 20
__dyn_pnt_amax_b = 13

__dyn_pnt_vmax_c = 20
__dyn_pnt_amax_c = 8

__dyn_pnt_vmax_d = 100
__dyn_pnt_amax_d = 20

__dyn_pnt_vmax = __dyn_pnt_vmax_a
__dyn_pnt_amax = __dyn_pnt_amax_a

__dif_vmax = 10
__dif_wmax = 0.3

__kin_car_vmax = 10
__kin_car_phimax = np.pi/4
__kin_car_La = 10
__kin_car_Lb = 30
__kin_car_Lc = 50

__dyn_car_vmax = 100
__dyn_car_amax = 20
__dyn_car_phimax = np.pi/4
__dyn_car_La = 10
__dyn_car_Lb = 50

#returns the list of edges that belong to a polygon
#edges are [[p1x,p1y],[p2x,p2y]]
def polyEdges(poly):
    edges = []
    for i in range(len(poly)):
        j = (i+1) % len(poly)
        edges.append([poly[i],poly[j]])
    return edges

#returns a map of all vertices mapped to their polygon indicies
#x,y -> lists of points in x and y dimensions
def vertsToPolys(x,y,button):
    vertices = {}
    o = 0
    for i in range(len(x)):
        vert = [x[i],y[i]]
        vertices[str(vert)] = o
        if button[i] == 3:
            o += 1
    return vertices

#returns the polygons mapped to their vertices
#x,y -> lists of points in x and y dimensions
def polysToVerts(x,y,button):
    polygons = {}
    p = 0
    polygons[p] = list()
    for i in range(len(x)):
        vert = [x[i],y[i]]
        polygons[p].append(vert)
        if button[i] == 3:
            p += 1
            polygons[p] = list()
    return polygons

#returns a list of LINE SEGMENTS(4 points) that all start from tracefrom and end at a vertex in x,y
#x,y -> lists of points in x and y dimensions
#edges -> list of edges in x,y index format
def visibiltyLines(tracefrom, x, y, edges, vertices):
    visible = []
    for i in range(len(x)):
        traceto = [x[i],y[i]]
        for edge in edges:
            intersects = False
            intersection = geom.find_intersection(tracefrom,traceto,[x[edge[0].astype(int)],y[edge[0].astype(int)]],[x[edge[1].astype(int)],y[edge[1].astype(int)]])
            if  intersection == None or intersection == 'collinear':
                continue
            else:
                if not vertices.has_key(str(intersection)):
                    intersects = True
                    break
        if intersects == False:
            visible.append([tracefrom,traceto])
    return visible

def travelableLines(tracefrom, start,goal,x,y,edges,button,plot = False):
    xl = x + [start[0]] #x.append(start[0])
    xl = xl + [goal[0]] #.append(goal[0])
    yl = y + [start[1]] #.append(start[1])
    yl = yl + [goal[1]] #.append(goal[1])
    bl = button + [3] + [3] #start and goal are their own polygons
    el = np.array(edges.tolist() + [[0.0+len(edges),0.0+len(edges)]] + [[0.0+len(edges),0.0+len(edges)]])
    vertices = vertsToPolys(xl,yl,bl)
    polygons = polysToVerts(xl,yl,bl)
    visible = visibiltyLines(tracefrom,xl,yl,el,vertices)
    if len(button) < 1:
        return visible
    travelable = []
    for line in visible:
        polyIdx = vertices[str(tracefrom)]
        polygon = polygons[polyIdx]
        testPt = geom.tracePoint(line[0],line[1],0.08)
        inside = geom.ptInPoly(polygon,testPt)
        if not inside:
            travelable.append(line)
    pedges = polyEdges(polygon)
    for pedge in pedges:
        if pedge[0] == tracefrom or pedge[1] == tracefrom:
            travelable.append(pedge)
    if plot:
        mh.draw2Dpolygons(xl,yl,el,False)
        plt.scatter(start[0],start[1], color = 'r')
        plt.scatter(goal[0],goal[1], color = 'g')
        #plt.scatter(tracefrom[0],tracefrom[1], marker = 'o')
        for l in travelable:
            plt.plot([l[0][0],l[1][0]],[l[0][1],l[1][1]], color = 'g', alpha = 0.1)
        #plt.show()
    return travelable


### A* for polygonal map ##############
class Node:

    pos = [-1,-1]
    cost = 9999
    parent = None

    def __init__(self,pos,cost,parent):
        self.pos = pos
        self.cost= cost
        self.parent= parent

    def toKey(self):
        return str(self.pos)

    def __str__(self):
        return str(self.pos)+"|"+str(self.cost)

def cmpNode(n1,n2):
    if n1.cost > n2.cost:
        return 1
    elif n1.cost == n2.cost:
        return 0
    else:
        return -1


def tracePath(node):
    current = node
    path = [current.pos]
    while current.parent != None:
        path.append(current.pos)
        current = current.parent
    return path


def growPoly(currentNode, start,goal,x,y,edges,button):
    res = []
    tracefrom = currentNode.pos
    travelable = travelableLines(tracefrom,start,goal,x,y,edges,button)
    for line in travelable:
        newpos = line[1] #[0] is the position of parent
        if not newpos == currentNode.pos:
            dist = geom.euclidDistance(newpos,goal) #distance to goal
            leng = geom.euclidDistance(line[0],line[1]) #lenght of this line
            newcost = currentNode.cost + dist + leng
            newnode = Node(newpos,newcost,currentNode)
            res.append(newnode)
    res.sort(cmp=cmpNode)
    return res


#start,goal,edges are np.array's, x,y,button are lists of floats
def astar(start,goal,x,y,edges,button,plot=True):
    if start.tolist()==goal.tolist():
        return [goal.tolist()]
    startNode = Node(start.tolist(),0,None)
    goalNode = Node(goal.tolist(),0,None)
    undiscovered = [startNode]
    discovered = {}
    goalReached = False
    while len(undiscovered) > 0 and goalReached == False:
        undiscovered.sort(cmp=cmpNode)
        undiscovered.reverse()
        current = undiscovered.pop()
        while discovered.has_key(current.toKey()):
            if len(undiscovered) == 0:
                return discovered
            else:
                current = undiscovered.pop()

        children = growPoly(current,start,goal,x,y,edges,button)

        for child in children:
            if discovered.has_key(child.toKey()):
                pass
            else:
                if child.pos[0] == goalNode.pos[0] and child.pos[1] == goalNode.pos[1]:
                    goalReached = True
                    path= tracePath(child)
                    path.reverse()
                    path=path[0:-1]
                    if plot:
                        plt.plot([start[0]]+[p[0] for p in path],[start[1]]+[p[1] for p in path], color = 'g', alpha = 0.1)
                    return path
                else:
                    undiscovered.insert(0,child)

        discovered[current.toKey()] = current

    return discovered


######### random tree stuff ##########


###### DYNAMIC STUFF




def modelTurningCircles(model):
    turn_radius = -1 #the radius that the model can make a cimplete circle in
    turn_center1 = [0,0]
    turn_center2 = [0,0]
    if model.__class__.__name__ == 'dyn_car' or model.__class__.__name__ == 'kin_car':
        turn_radius = model.L/np.tan(model.max_ang)
    else:
#        print 'MODEL HAS NO TURNING CIRCLE'
        return None,None,None

    if model.orientation[0] == 0 and model.orientation[1] != 0: #model looking directly UP
        turn_center1 = [turn_radius, 0.0]
        turn_center2 = [-turn_radius, 0.0]
    elif model.orientation[1] == 0 and model.orientation[0] != 0: #model looking directly RIGHT
        turn_center1 = [0.0, turn_radius]
        turn_center2 = [0.0, -turn_radius]
    elif model.orientation != [0,0,0]:
        model_slope = model.orientation[1]/model.orientation[0]
        print model_slope
        a1 = np.sqrt( turn_radius**2 / model_slope**2 +1 )
        a2 = -a1 #np.sqrt returns positive all the time.
        b1 = model_slope * a1
        b2 = model_slope * a2

        turn_center1 = [a1*turn_radius,b1*turn_radius] #the two centers for the model to make a turn around
        turn_center2 = [a2*turn_radius,b2*turn_radius]
    else:
        print 'wtf?'
    turn_center1 = np.array(turn_center1) + np.array([model.position[0],model.position[1]])
    turn_center2 = np.array(turn_center2) + np.array([model.position[0],model.position[1]])
    return turn_center1, turn_center2, turn_radius


# generates random valid points around each ideal path waypoint
def generateWPcandidates(ideal_path, polys, cand_cnt, cand_radius):
#    res = [[] for i in range(len(ideal_path))]
    res = []
    for k in range(len(ideal_path)):
        res.append(ideal_path[k])
        for i in range(cand_cnt):
            newpt = np.random.uniform(low = -cand_radius, high = cand_radius, size = 2) + np.array(ideal_path[k])
            while not geom.validPoint(polys,newpt):
                newpt = np.random.uniform(low = -cand_radius, high = cand_radius, size = 2) + np.array(ideal_path[k])
            res.append(newpt.tolist())
    return np.array(res)

#points are the waypoints the copies of the model will go to
def samplePoints(points, model, polys):
    res = {}
    c1,c2,r = modelTurningCircles(model)
    for p in points:
        if c1 != None:
            if geom.euclidDistance(p,c1) < r or geom.euclidDistance(p,c2) < r: #if the point is not reachable without loops
                pass
        sample = copy.deepcopy(model)
        sample.view.reinit()
        sample.view.cont = False # Else this takes forever
        if model.__class__.__name__ == 'dyn_car':
            sample.followWaypoints([p],0,False)
        elif model.__class__.__name__ == 'dyn_point':
            sample.followWaypoints([p],False)
        elif model.__class__.__name__ == 'kin_car':
            sample.followWaypoints([p])
        elif model.__class__.__name__ == 'DynamicPoint': #the new dynamic point
            model.followWaypoints([p])

        else:
            print 'UNDEFINED MODEL, CANT SAMPLE'
            return None
        crash = False
        for trace_pt in sample.view.pos_trace:
            if not geom.validPoint(polys,trace_pt):
                crash = True
                break
        if not crash:
            res[str(p)] = sample
    return res


class SampleNode:
    model = None
    cost = None
    parent = None
    pos = None

    def __init__(self,pos,model,cost,parent):
        self.model = model
        self.cost = cost
        self.parent = parent
        self.pos = pos


    def __str__(self):
        return 'sampleNode:ori;'+str(self.model.orientation[2])+',pos:'+str(self.pos) + ',cost:' + str(self.cost)

    def toKey(self):
#        return str([self.pos,self.model.orientation])
        return str(self.pos)


def growSample(current_node, targets, end_goal, polys):
    res = []
    result_models = samplePoints(targets,current_node.model, polys)
    for target in targets:
        if result_models.has_key(str(target)):
            resmod = result_models[str(target)]
            cost = geom.euclidDistance(resmod.position,end_goal) #+ current_node.cost + len(resmod.view.pos_trace)
            newnode = SampleNode(target,resmod,cost,current_node)
            res.append(newnode)
    res.sort(cmp=cmpNode)
    return res

def sampleSearch(start, goal, ideal_path, polys, cnd_cnt, cnd_rad, model):
    ## generate the random points around the ideal path
    cands = generateWPcandidates(ideal_path, polys, cnd_cnt, cnd_rad)
    ## plot the random points
    plt.scatter(cands[:,0],cands[:,1], alpha = 0.1)
    ## creat a node for the root of the search tree
    startNode = SampleNode(start,model,0,None)
    ## a priority queue to keep the nodes in. high prio = low cost
    undiscovered = [startNode]
    ## set of discovered nodes
    discovered = {}
    ## flag to stop searching
    goalReached = False
    ## while there are undiscovered nodes and we have not found the goal yet
    while len(undiscovered) > 0 and not goalReached:
        ## sort the list by costs
        undiscovered.sort(cmp=cmpNode)
        ## the first element is the least costly, explore first
        current = undiscovered.pop()
#        print '\ncurrent:' + str(current.toKey())
        #check if this node was discovered, find one that is not
        while discovered.has_key(current.toKey()):
            if len(undiscovered) == 0:
                print 'out of undiscovered nodes, returning discovered'
                return discovered
            else:
                current = undiscovered.pop()
        #generate the child nodes of this node
        children = growSample(current,cands,goal,polys)
#        print 'chldlen;'+str(len(children))
        ## for each child
        for child in children:
            vw.drawPath(child.model.view.pos_trace, a = 0.1)
            ## check if it has been discovered, find one that is not
            if discovered.has_key(child.toKey()):
                pass
            else:
                pos = child.model.position
                distToGoal = geom.euclidDistance(pos,goal)
#                print 'childDistToGoal;' + str(distToGoal)
#                if pos[0] == goal[0] and pos[1] == goal[1]:
                ## if the child is within an acceptable distance from the goal, we are done
                if distToGoal < 5:
                    print 'FOUND GOAL'
                    goalReached = True
                    wps = []
                    tracing = child
                    while tracing.parent != None:
                        print str(tracing)
                        wps.append(tracing.pos.tolist())
                        tracing = tracing.parent
                    print 'found path'
                    wps.reverse()
                    return np.array(wps)
                else:
                    undiscovered.insert(0,child)
        discovered[current.toKey()] = current

        print 'lendiscovered;' + str(len(discovered))
        print 'lenundisc;' + str(len(undiscovered))
    print 'last resort'
    return discovered


###########
if __name__ == '__main__':
    s, g, x, y, edges, button = mh.generateOBJstr('polygObstTest.mat')
    polygons = polysToVerts(x,y,button)
    visRes = astar(s,g,x,y,edges,button)


    view = vw.TracingView(x,y,edges)
    test_model = md.dyn_car(view,100, 20,np.pi/4,20)
    test_model.setPosition(geom.in3d(s))

    p1,p2,r = modelTurningCircles(test_model)
    c1 = plt.Circle(p1,r, fill = False)
    c2 = plt.Circle(p2,r, fill = False)
    fig = plt.gcf()
    fig.gca().add_artist(c1)
    fig.gca().add_artist(c2)

#    cands = generateWPcandidates(visRes, polygons, 5, 15)
#    plt.scatter(cands[:,0],cands[:,1])

    searchRes = sampleSearch(s,g,visRes,polygons,10,20,test_model)

    plt.scatter(searchRes[:,0],searchRes[:,1], color = 'r', marker = 'x')

    view = vw.TracingView(x,y,edges)
    timer_model = md.dyn_car(view,100, 20,np.pi/4,20)
    timer_model.setPosition(geom.in3d(s))
    t = timer_model.followWaypoints(searchRes.tolist(),False)
    vw.drawPath(timer_model.view.pos_trace)
    print t

#    sampleRes = samplePoints(cands[0].tolist(),test_model, polygons)
#    for samp in sampleRes:
#        nextRes = samplePoints(cands[1].tolist(),samp, polygons)
#        vw.drawPath(samp.view.pos_trace)
#        for nextsamp in nextRes:
#            vw.drawPath(nextsamp.view.pos_trace)

#    startNode = SampleNode(s,test_model,0,None)
#    str(startNode)
#    gsmpl = growSample(startNode,cands,g,polygons)
#    for sampl in gsmpl:
#        print str(sampl)