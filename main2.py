# -*- coding: utf-8 -*-
"""
Created on Fri Feb 19 22:27:10 2016

@author: kkalem
"""


import matlabHandler as mh
import polySearch as ps
import models as md
import matplotlib.pyplot as plt
import views as vw
import geometry as geom


__pnt_vel = 100.
__pnt_acc = 1.
__car_vel = 999. #not given
__car_acc = 1.
__car_phi = 1.
__car_len = 1. #not given

if __name__ == "__main__":
    s,g,x,y,edges,button,customer = mh.generateOBJstr('assn2map1.mat') #assn2map1 or map2
    mh.draw2Dpolygons(x,y,edges,False)
    try:
        plt.scatter(customer[:,0],customer[:,1],marker = 's')
    except IndexError:
        pass
    plt.scatter(s[:,0],s[:,1],marker = 'x', color = 'g')
    plt.scatter(g[:,0],g[:,1],marker = 'v', color = 'r')
    for i in range(len(s)):
        plt.plot([s[i,0],g[i,0]],[s[i,1],g[i,1]],alpha = 0.1, color = 'b')

    for i in range(len(s)):
        st = s[i]
        gt = g[i]

        visRes = ps.astar(st,gt,x,y,edges,button)
        
        polygons = ps.polysToVerts(x,y,button)

        view = vw.TracingView()
#        model = md.dyn_car(view,__car_vel, __car_acc,__car_phi,__car_len)
        model = md.dyn_point(view,__pnt_vel,__pnt_acc)
        model.setPosition(geom.in3d(st))
        searchRes = ps.sampleSearch(st,gt,visRes,polygons,10,10,model)

#        model = md.dyn_car(view,__car_vel, __car_acc,__car_phi,__car_len)
        model = md.dyn_point(view,__pnt_vel,__pnt_acc)
        model.setPosition(geom.in3d(st))
        model.view.reinit()
#        a,path_length,vel1=model.followWaypoints(searchRes.tolist(),0,False)
        t =model.followWaypoints(searchRes.tolist(),False)
        vw.drawPath(model.view.pos_trace)
        plt.scatter(searchRes[:,0],searchRes[:,1], color = 'r', marker = 'x')