# -*- coding: utf-8 -*-
"""
Created on Sun Mar 06 19:43:02 2016

@author: kkalem
"""
import matplotlib.pyplot as plt

import views
import matlabHandler as mh
import dynamic_car as dc
import geometry as geom

plt.axis('equal')
s,g,x,y,edges,button,customer = mh.generateOBJstr('assn2map2.mat') #assn2map1 or map2
#mh.draw2Dpolygons(x,y,edges,False)


#test_center = dp.DynamicPoint(view = views.TracingView(), ID=100, X = [0.,0.], max_v = 10.)
agents = []
for i in range(12):
    view = views.TracingView()
    model = dc.DynamicCar(view, ID=i, X = s[i])
    model.addTarget(g[i])
    model.view.reinit()
    agents.append(model)

for agent in agents:
    r = agent.size
    c1 = plt.Circle(agent.X,r, fill = False)
    fig = plt.gcf()
    fig.gca().add_artist(c1)
#    agent.addAvoid(test_center)
    for otherAgent in agents:
        if agent.ID != otherAgent.ID:
            agent.addAvoid(otherAgent)

plt.scatter(g[:,0],g[:,1], marker = 'x', alpha = 0.5)

for time in range(40):
#    print time
    results = ''
    for agent in agents:
        result = agent.tick()
        results+=result
#        plt.plot([agent.X[0],agent.X[0]+agent.A[0]],[agent.X[1],agent.X[1]+agent.A[1]], color = 'b', alpha = 0.1)
#        plt.plot([agent.X[0],agent.X[0]+agent.V[0]],[agent.X[1],agent.X[1]+agent.V[1]], color = 'g', alpha = 0.1)
        for otherAgent in agents:
            if agent.ID != otherAgent.ID and geom.norm(agent.X - otherAgent.X) <= 2:
                plt.scatter(agent.X[0],agent.X[1],marker = 'v', color = 'r')
#    print results
    if results == 'done'*len(agents):
        print 'COMPLETED IN', time*agents[0].view.getTimeStep(), 'SECONDS'
        break
    if time%30 == 0:
        plt.pause(0.001)


plt.scatter(g[:,0],g[:,1], marker = 'x', alpha = 0.5)
plt.scatter(s[:,0],s[:,1], marker = 'v', alpha = 0.5)
for agent in agents:
    views.drawPath(agent.view.pos_trace)