# -*- coding: utf-8 -*-
"""
Created on Sun Mar 06 19:29:48 2016

@author: kkalem
"""
import matlabHandler as mh
import views as vw
import formation as form
import dynamic_car as dc
import geometry as geom
import vrep
import numpy as np

import matplotlib.pyplot as plt

s,g,x,y,edges,button,customer = mh.generateOBJstr('assn2map1.mat') #assn2map1 or map2

starting = s.tolist() + g.tolist()

doInVrep=False
scale=0.01
baseName='port_sphere'

starting = s.tolist() + g.tolist()
if doInVrep:
    cid = vw.startVrep()
    err, handle = vrep.simxGetObjectHandle(cid, baseName, vrep.simx_opmode_oneshot_wait)
    objHandle=[handle]
    for st in starting:
        err, newFloorHandle = vrep.simxCopyPasteObjects(cid, [objHandle[0]] , vrep.simx_opmode_oneshot_wait)
        vrep.simxSetObjectPosition(cid, newFloorHandle[0], -1, (np.array(st)*scale).tolist(), vrep.simx_opmode_oneshot)
        objHandle.extend(newFloorHandle)
    view = vw.vrep_view(cid,'',scale)
else:
    view = vw.TracingView()
fx = 0.
fy = 0.
for i in range(len(starting)):
    fx += starting[i][0]
    fy += starting[i][1]
fx = fx / len(starting)
fy = fy / len(starting)


f = form.formation([[0,0],[1,0],[2,0],[3,0],[0,1],[1,1],[2,1],[3,1],[1,2],[2,2]],d=50, view = view, velocity = [0,10], pos = [fx-100,fy-70])

agents = []

i=0
for pos in starting:
    if doInVrep:
        view = vw.vrep_view(cid,objHandle[i+1],scale)
    else:
        view = vw.TracingView(init_pos = geom.in3d(pos), cont = False)
    agent = dc.DynamicCar(view, 0, X = pos)
    agent.view.reinit()
    agents.append(agent)
    i+=1

if not doInVrep:
    for agent in agents:
        r = agent.size
        c1 = plt.Circle(agent.X,r, fill = False)
        fig = plt.gcf()
        fig.gca().add_artist(c1)

for time in range(2500):
#    plt.clf()
    modelposs = [agents[k].X for k in range(len(agents))]
    indices = f.compute(modelposs)
    for j in range(len(agents)):
        agents[j].setTarget(f.real[indices[j][1]])
        agents[j].tick()
#        plt.scatter(agents[j].X[0],agents[j].X[1])
    f.tick()
#    if time%30==0 and not doInVrep:
#        plt.pause(0.001)
#        f.draw()

for agent in agents:
    vw.drawPath(agent.view.pos_trace)
    plt.scatter(agent.X[0], agent.X[1])
f.draw()