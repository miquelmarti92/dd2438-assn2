# -*- coding: utf-8 -*-
"""
Created on Wed Jan 27 12:37:31 2016

@author: Miquel
"""

import time
import numpy as np
import views as vw

__THRESH__ = 0.5

def normalize(v):
    norm=np.linalg.norm(v)
    if norm==0:
       return v
    return v/norm


class model_base:

    def __init__(self, view):
        self.view=view
        self.position=self.view.getPosition()
        self.orientation=self.view.getOrientation()
        self.dt=self.view.getTimeStep()
        self.timer = 0.

    def setPosition(self,p_vector):
        self.position = np.array(p_vector)

    def setOrientation(self,o_vector):
        self.orientation = np.array(o_vector)

    def updatePosition(self,u_vector):
        self.position = np.array(self.position)+np.array(u_vector)

    def updateOrientation(self,u_vector):
        self.orientation = np.array(self.orientation)+np.array(u_vector)

    def nextTimeStep(self):
        self.timer+=self.dt


class dyn_point(model_base):

    def __init__(self,view = vw.TracingView(), max_velocity = 100. ,max_accel = 1. ,init_pos = [0,0,0]):
        self.velocity = np.array([0.,0.])
        self.max_accel = max_accel
        self.max_velocity = max_velocity
        model_base.__init__(self,view)
        self.setPosition(init_pos)
        self.view.reinit()
        self.goal=self.position
        self.not_at_goal = False
        self.brake_flag=True
        self.brake_cond=False
        self.t_vel= self.max_accel*5./self.max_velocity #PLAY WITH THAT

    def applyAccel(self,accel_vec):
        #add new velocity vector
        self.velocity+=accel_vec*self.dt
        #Max velocity condition
        if np.linalg.norm(self.velocity)>self.max_velocity:
            self.velocity=normalize(self.velocity)*self.max_velocity
        # average of the velocities before and after adding in the accel
        avg_vel=self.velocity-accel_vec*self.dt/2
        #Update position according to avg current vel
        self.updatePosition(np.hstack([self.dt*avg_vel,0.]))

    def setGoal(self,p,target_vel,brake_flag):
        global __THRESH__
        self.goal=p
        u_vec=np.array(self.goal)-self.position[0:2]
        self.not_at_goal = np.linalg.norm(u_vec)>max(np.linalg.norm(self.velocity)*self.dt,__THRESH__)
        self.brake_flag=brake_flag
        self.brake_cond=False
        self.t_vel=target_vel
        print "NEW:Goal:"+str(self.goal)+" Brake:"+str(self.brake_flag)+" Tvel:"+str(self.t_vel)

    def tick(self):
        global __THRESH__
        #Vectors to goal points
        u_vec=np.array(self.goal)-self.position[0:2]

        #Vel target vector
        vprime = u_vec
        if np.linalg.norm(vprime) > self.max_velocity:
            vprime = normalize(vprime) * self.max_velocity
        a = normalize(vprime - self.velocity) * self.max_accel

        #TODO Braking
        if self.brake_flag==True:
            brake_dist=np.power(np.linalg.norm(self.velocity)-self.t_vel,2.)/(self.max_accel*2.)
            if self.brake_cond:
                a=normalize(a-normalize(self.velocity)*self.max_accel*2)*self.max_accel
#                print "Braking! Current vel:" + str(np.linalg.norm(self.velocity))
            else:
                self.brake_cond = np.abs(np.linalg.norm(u_vec)) <= brake_dist + np.linalg.norm(self.velocity)*self.dt


        #Apply acceleration
        self.applyAccel(a)
        u_vec=np.array(self.goal)-self.position[0:2]

        self.not_at_goal = np.linalg.norm(u_vec)>max(np.linalg.norm(self.velocity)*self.dt,__THRESH__)
        #Next time step
        self.nextTimeStep()

    def tickAndSync(self):
        self.tick()
        self.view.setPosition(self.position)
        self.view.syncSignal()


    #This function implements the control over the model
    #Computes and applies accel vectors to model, updates view
    #TODO add brake flag thingy
    def followWaypoints(self,waypoints,stop_flag):

        #Init values
        i=1

        for p in waypoints:
            #If stop_flag, stop at last point
            if i==len(waypoints) and stop_flag:
                target_max_vel=0
            else:
                target_max_vel= self.max_accel*5./self.max_velocity
            #Set goal and target maximum velocity
            #Might want to change dyn if braking to achieve target max vel or not?
            self.setGoal(p,target_max_vel,self.brake_flag)

            #Step in direction to new point
            while self.not_at_goal:
                #Advance time in model
                self.tick()
                #Update view
                self.view.setPosition(self.position)
                #Refresh view
                self.view.syncSignal()

            i+=1

        return self.timer

class dyn_car(model_base):
#TODO change controller and add basic control input methods to interact from controller
#TODO apply acceleration and angle at wheels

    def __init__(self,view,max_velocity,max_accel,max_ang, length):
        self.L = length
        self.velocity=0
        self.max_accel = max_accel
        self.max_ang = max_ang
        self.max_velocity = max_velocity
        self.reset_velocity = self.max_velocity
        model_base.__init__(self,view)
        self.step_size=self.velocity*self.dt
        self.step_size_ang=self.velocity/self.L*np.tan(self.max_ang)*self.dt
        self.vel_vec=[]
        self.path=0
        self.brake_flag=False

    def setGoal(self,p,target_vel,path_length,brake_flag):
        global __THRESH__
        self.goal=p
        u_vec=np.array(self.goal)-self.position[0:2]
        self.not_at_goal = np.linalg.norm(u_vec)>max(np.linalg.norm(self.velocity)*self.dt,__THRESH__)
        self.brake_flag=brake_flag
        self.brake_cond=False
        self.t_vel=target_vel
        self.path_length=path_length
        print "NEW:Goal:"+str(self.goal)+" Brake:"+str(self.brake_flag)+" Tvel:"+str(self.t_vel)

    #TODO recheck braking thingy
    def tick(self):
        global __THRESH__
        #Vectors to goal points
        u_vec=np.array(self.goal)-self.position[0:2]
        #Compute target angle
        target_angle=np.arctan2(u_vec[1],u_vec[0])
        #Required rotation
        theta=target_angle-self.orientation[2]
        #Account for both directions of rotation,shortest
        if abs(theta)>np.pi:
            theta=theta-np.sign(theta)*2*np.pi
        #Limit rotation according to max ang step size
        if abs(theta)>self.step_size_ang:
            theta=np.sign(theta)*self.step_size_ang
        #Update orientation
        self.updateOrientation([0,0,theta])

        #Add vel step towards dest
        self.velocity=self.velocity+self.max_accel*self.dt
        #Compute break distance
        brake_dist=np.power(self.velocity-self.t_vel,2)/2./self.max_accel
        #Brake if last waypoint and under brake dist plus one step size
        if abs(self.path_length-self.path)<abs(brake_dist) and self.brake_flag==True:
            self.velocity=self.velocity-self.max_accel*2.*self.dt
            print "Curr vel: " + str(np.linalg.norm(self.velocity))

        #Limit max velocity
        if self.velocity>self.max_velocity:
            self.velocity=self.max_velocity
        self.step_size=self.velocity*self.dt
        self.vel_vec=self.vel_vec+[self.velocity]

        #Update position
        self.updatePosition([self.velocity*self.dt*np.cos(self.orientation[2]),self.velocity*self.dt*np.sin(self.orientation[2]),0])
        self.path+=self.velocity*self.dt

        #New vect to goal
        u_vec=np.array(self.goal)-self.position[0:2]
        #Max angle change x frame
        self.step_size_ang=self.velocity/self.L*np.tan(self.max_ang)*self.dt
        #TODO RECHECK THIS
        self.not_at_goal = np.linalg.norm(u_vec)>max(np.linalg.norm(self.velocity)*self.dt,__THRESH__)
        #Next time step
        self.nextTimeStep()

    def followWaypoints(self,waypoints,path_length,brake_flag):
        i=1
        target_max_vel=0
        self.path=0
        for p in waypoints:

            #Set goal and target maximum velocity
            #Might want to change dyn if braking to achieve target max vel or not?
            self.setGoal(p,target_max_vel,path_length,brake_flag)

            #Step in direction to new point
            while self.not_at_goal:

                self.tick()
                #Update views
                self.view.setOrientation(self.orientation)
                self.view.setPosition(self.position)
                #Next time step
                self.nextTimeStep()


            i+=1

        return self.timer, self.path, self.vel_vec
#################################################################
#################################################################
#################################################################



if __name__ == "__main__": #so these dont work when simply importing this file but work when you run this file
#    cid = vw.startVrep()
    view = vw.TracingView()
#    view = vw.vrep_view(cid,'Cuboid',0.1)
    model = dyn_point(view,10.,1.)
#    model = dyn_car(view,100,1,np.pi/4,10)
    model.setPosition([0,0,0.05])

    time.sleep(2) # take your time switch to vrep, inspire, expire, no stress

    #Vector of waypoints
    waypoints=[[10,30]]

    #Draw waypoints
    view.drawPoints(waypoints)

    #Follow waypoints
    #DYN POINT
    t=model.followWaypoints(waypoints,True)

    #DYN CAR
#    tmodel=copy.deepcopy(model)
#    t,p,v=tmodel.followWaypoints(waypoints,0,False) #Test model to compute path length
#    t,p,v=model.followWaypoints(waypoints,p,True)


    print "Time taken: "+str(t)
#    vw.stopVrep(cid)

