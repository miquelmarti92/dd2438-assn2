# -*- coding: utf-8 -*-
"""
Created on Sun Feb 21 23:46:28 2016

@author: Miquel
"""

import vrep
import sys
import numpy as np
import matplotlib.pyplot as plt

__plotview_time_step = 0.03

#################################################################
#################################################################
#################################################################

def startVrep():
    vrep.simxFinish(-1) # just in case, close all opened connections
    clientID = vrep.simxStart('127.0.0.1',19999,True,True,5000,5) # Connect to V-REP
    vrep.simxSynchronous(clientID,True)
    vrep.simxStartSimulation(clientID,vrep.simx_opmode_oneshot)
    if clientID != -1:
        return clientID
        print 'Connected'
    else:
        print 'Can not connect to v-rep'
        sys.exit('Connection failed')

def stopVrep(clientID):
    # Before closing the connection to V-REP, make sure that the last command sent out had time to arrive. You can guarantee this with (for example):
    vrep.simxGetPingTime(clientID)
    # Now close the connection to V-REP:
    vrep.simxFinish(clientID)

#initilazies the vrep connection, runs the given mainFunc, terminates the connection
def runInVrep(mainFunc):
    clientID = startVrep()
    mainFunc(clientID)
    stopVrep(clientID)

#################################################################
#################################################################
#################################################################


class vrep_view:
    cid = -1
    baseHandle = -1
    scaling=-1

    pos_trace = [] #to hold the position of a model in time
    ori_trace = []

    time = 0. #to keep track of time

    def __init__(self, cid, baseName, scaling):
        self.cid = cid
        if isinstance(baseName, basestring):
            err, self.baseHandle = vrep.simxGetObjectHandle(cid, baseName, vrep.simx_opmode_oneshot_wait)
        else:
            self.baseHandle=baseName
        self.scaling = scaling

    def reinit(self):
        self.pos_trace = []
        self.ori_trace = []

    #returns the base's position as a vector [x,y,z]
    def getPosition(self):
        err, robotPos = vrep.simxGetObjectPosition(self.cid, self.baseHandle, -1, vrep.simx_opmode_oneshot_wait)
        if err == 0:
            return np.array([robotPos[0]/self.scaling,robotPos[1]/self.scaling,robotPos[2]])
        else:
            print 'Error: '+str(err)+' cannot get position'
            return [0,0,0]

    #returns the base's orientation as a vector [alpha, beta, gamma]
    def getOrientation(self):
        err, robotOri = vrep.simxGetObjectOrientation(self.cid, self.baseHandle, -1, vrep.simx_opmode_oneshot_wait)
        if err == 0:
            return robotOri
        else:
            print 'Error: '+str(err)+' cannot get orientation'
            return [0,0,0]

    def setPosition(self,p_vector):
        err = vrep.simxSetObjectPosition(self.cid, self.baseHandle, -1,np.array([self.scaling*p_vector[0],self.scaling*p_vector[1],0.05]), vrep.simx_opmode_oneshot)
        self.pos_trace.append(p_vector) #save the position for later
        if err != 0:
            print 'Error: '+str(err)+' cannot set position'

    def setOrientation(self,o_vector):
        err = vrep.simxSetObjectOrientation(self.cid, self.baseHandle, -1,np.array(o_vector), vrep.simx_opmode_oneshot)
        self.ori_trace.append(o_vector)
        if err != 0:
            print 'Error: '+str(err)+' cannot set orientation'

    def syncSignal(self,time=0):
        #Sync signal
#        if time == self.time: #agent's time and views time are the same, will probably act like streaming.
#TODO: needs testing?
        vrep.simxSynchronousTrigger(self.cid) #sync with vrep
#            self.time += 1 #time moves for the view


    def getTimeStep(self):
        err, dt = vrep.simxGetFloatingParameter(self.cid, vrep.sim_floatparam_simulation_time_step, vrep.simx_opmode_oneshot_wait)
        if err == 0:
            return dt
        else:
            print 'Error: '+str(err)+' cannot get sim time step'
            return [0,0,0]

    def drawPoints(self,points):
        for p in points:
            err,dummyhandle = vrep.simxCreateDummy(self.cid,0.1,None,vrep.simx_opmode_oneshot_wait)
            err = vrep.simxSetObjectPosition(self.cid, dummyhandle, -1,self.scaling*np.hstack([p,0]), vrep.simx_opmode_oneshot_wait)
            if err != 0:
                print 'Error: '+str(err)+' cannot draw point'

#################################################################
#################################################################
#################################################################

def waypointsToPathLines(wps):
    lines = []
    for i in range(1,len(wps)):
        lines.append([wps[i-1],wps[i]])
    return lines

def drawPath(trace, c = 'r', a = 0.3):
    pathlines = waypointsToPathLines(trace)
    for line in pathlines:
        line = np.array(line)
        plt.plot([line[0][0],line[1][0]],[line[0][1],line[1][1]],color = c, alpha = a)

def timestep():
    return __plotview_time_step
#################################################################
#################################################################
#################################################################


class TracingView:
    cid = -1
    baseHandle = -1

    ori = np.array([1.,0,0])
    pos = np.array([0,0,0])
    pos_trace = []
    ori_trace = []
    polys = None
    cont = True
    timecount = 0
    speed_mult = 100

    def __init__(self, cont = True, speed_mult = 100, init_pos = [0,0,0]):
        self.pos = init_pos
        self.pos_trace = []
        self.cont = cont
        self.speed_mult = speed_mult
        self.pos = np.array(init_pos)

    def reinit(self):
        self.pos_trace = []
        self.ori_trace = []
        self.timecount = 0


    #returns the base's position as a vector [x,y,z]
    def getPosition(self):
        return self.pos

    #returns the base's orientation as a vector [alpha, beta, gamma]
    def getOrientation(self):
        return self.ori

    def setPosition(self,p_vector):
        self.pos = p_vector
        self.pos_trace.append(self.pos)
        if self.cont:
            if len(self.pos_trace) >= 2:
                plt.plot([self.pos_trace[-2][0],self.pos[0]],[self.pos_trace[-2][1],self.pos[1]], color = 'r', alpha = 0.3)
            else:
                plt.scatter(self.pos[0],self.pos[1])
            self.timecount += 1.
        return self.pos

    def getTraces(self):
        return self.pos_trace, self.ori_trace

    def setOrientation(self,o_vector):
        self.ori = o_vector
        self.ori_trace.append(self.ori)
        return self.ori

    def syncSignal(self,time=0):
        if self.cont:
            if self.timecount%self.speed_mult == 0:
                plt.pause(0.001)
        return True

    def getTimeStep(self):
        return timestep()

    def drawPoints(self,points = None):
        p = np.array(points)
        plt.scatter(p[:,0],p[:,1],marker = 'x', alpha = 0.1)
        return True

