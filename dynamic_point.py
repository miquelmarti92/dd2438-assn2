# -*- coding: utf-8 -*-
"""
Created on Thu Feb 25 18:25:18 2016

@author: kkalem

"""

import numpy as np
import matplotlib.pyplot as plt

import views
import geometry as geom

class DynamicPoint():
    def __init__(self,view, ID, X = [0.,0.], V = [0.,0.], A = [0.,0.], max_v = 100., max_a = 1., size = 1.):
        self.ID = ID

        #physics
        self.X = np.array(X)
        self.V = np.array(V)
        self.A = np.array(A)
        self.dt = view.getTimeStep()

        self.max_a = max_a
        self.max_v = max_v

        #other stuff
        self.view = view
        self.target = self.X #target point this agent will strive to reach
        self.target_list = []
        self.avoid_list = [] #list of points to avoid when moving
        self.sim_time = 0.

        self.veh_in_range = {}

        self.size = size

        self.__braking_factor = 150. #required because of the accel and vel. limitations, tied to angle
        self.__target_range = self.size
        self.__coll_factor = 20. #how aggressively do we want to avoid collisions
        self.__stop_at_last_target = True

#        self.Vprimes = []
        self.Vavoids = []
        self.Vtarget = []
        self.Vbrakes = []

        self.__we_are_done_here = False

        self.__vel_obs_time = 100. #time in ticks for look-ahead for other agents
        self.velocity_obstacle = self.__generate_vel_obs(self.__vel_obs_time) #a cut-off cone
        self.bounding_box = self.__generate_bounding_box() #a box around the disc
        self.__detection_obstacle = self.__generate_detection_obs(self.__vel_obs_time) #a possibly different cone that is at most brake-dist long

        self.__wall_time = 30. # time in ticks for look-ahead for walls
        self.wall_list = [] #list of walls in the map to avoid

        #### compatibilty stuff ####
        self.position = self.X

    #generates a box that encompasses this agent when not moving
    def __generate_bounding_box(self):
        #no 'heading', make a bounding box
        p1 = self.X + [self.size , self.size] #forward, up
        p2 = self.X + [self.size , -self.size] #forward, down
        p4 = self.X + [-self.size , self.size] #backward, down
        p3 = self.X + [-self.size , -self.size] #backward, up

        return np.array([[p1,p2],[p2,p3],[p3,p4],[p4,p1]])

    #generates a parallelogram that represents the most likely places this agent will be in look_ahead time
    def __generate_vel_obs(self,look_ahead):
        if geom.norm(self.V) == 0:
            #no 'heading', make a bounding box
            return self.__generate_bounding_box()
        else:
            #heading is velocity, make a rectangle
            norm_self_V = geom.normalize(self.V)
            perp_self_V = geom.normalize(geom.perp_vec(self.V))

            p4 = self.X + perp_self_V*self.size - self.size*norm_self_V #backward, down
            p3 = self.X - perp_self_V*self.size - self.size*norm_self_V #backward, up
            biggest_change = self._constrainV((self.max_a*perp_self_V) + self.V)
            p1base = self.X + norm_self_V*self.size + perp_self_V*self.size
            p2base = self.X + norm_self_V*self.size - perp_self_V*self.size
            forward = (self.V * look_ahead)
            p1 = p1base + forward + biggest_change #forward, up
            p2 = p2base + forward - biggest_change#forward, down

        return np.array([[p1,p2],[p2,p3],[p3,p4],[p4,p1]])

    #generates a parallelogram that this agent will use to avoid any other object.
    #same as vel. obs. except this is capped at brake distance.
    def __generate_detection_obs(self,look_ahead):
        if geom.norm(self.V) == 0:
            #no 'heading', make a bounding box
            return self.__generate_bounding_box()
        else:
            #heading is velocity, make a rectangle
            norm_self_V = geom.normalize(self.V)
            perp_self_V = geom.normalize(geom.perp_vec(self.V))

            p4 = self.X + perp_self_V*self.size - self.size*norm_self_V #backward, down
            p3 = self.X - perp_self_V*self.size - self.size*norm_self_V #backward, up
            biggest_change = self._constrainV((self.max_a*perp_self_V) + self.V)
            p1base = self.X + norm_self_V*self.size + perp_self_V*self.size
            p2base = self.X + norm_self_V*self.size - perp_self_V*self.size
            forward = (self.V * look_ahead)
            brake_dist = (geom.norm(self.V)**2) / (2* self.max_a)
            if geom.norm(forward) > brake_dist: #dont look further than your brake distance
                forward = geom.normalize(forward)*brake_dist
            p1 = p1base + forward + biggest_change #forward, up
            p2 = p2base + forward - biggest_change#forward, down

        return np.array([[p1,p2],[p2,p3],[p3,p4],[p4,p1]])


    def tick(self):
#        if self.__we_are_done_here: return 'done'
        #list of velocities this agent ideally wants to have
#        self.Vprimes = []
        self.Vavoids = []
        self.Vtarget = []
        self.Vbrakes = []

        #update boxes
        self.velocity_obstacle = self.__generate_vel_obs(self.__vel_obs_time)
        self.bounding_box = self.__generate_bounding_box()
        self.__detection_obstacle = self.__generate_detection_obs(self.__vel_obs_time)

####### AVOIDANCE #########
        if not self.__we_are_done_here:
            #agent avoidance first
            for B in self.avoid_list:
                intersections = []
                for segment in self.__detection_obstacle.tolist():
                    other_obstacle = B.velocity_obstacle.tolist()
                    other_obstacle.extend(B.bounding_box.tolist())
                    for other_segment in other_obstacle:
                        intersections.append(geom.find_intersection(segment[0],segment[1],other_segment[0],other_segment[1]))

####                mindist = 9999
#                closest_sect = None
#                for sect in intersections:
#                    if sect != 'collinear' and sect is not None:
#                        dist = geom.ptToLineSegment(self.X.tolist(),B.X.tolist(),sect)
#                        if dist < mindist:
#                            closest_sect = sect
#                            mindist = dist
#
#                if closest_sect is not None:
#                    Vprime = geom.perp_vec((self.X - closest_sect)*self.__coll_factor)
####                    self.Vavoids.append(Vprime)
                colls = []
                for sect in intersections:
                        if sect != 'collinear' and sect is not None:
                             colls.append(sect)

                for coll in colls:
                    Vprime = geom.perp_vec((self.X - coll)*self.__coll_factor)
                    Vprime2 = (self.X - coll)*self.__coll_factor
                    self.Vavoids.append(Vprime)
                    self.Vavoids.append(Vprime2)


            #wall avoidance second
            self.velocity_obstacle = self.__generate_vel_obs(self.__wall_time)
            intersections = []
            for wall in self.wall_list:
                for segment in self.velocity_obstacle.tolist():
                    intersection = geom.find_intersection(segment[0],segment[1],wall[0],wall[1])
                    intersections.append([intersection,wall])

            for sect in intersections:
                wall = sect[1]
                intersection = sect[0]
                if intersection != 'collinear' and intersection is not None:
                    Vprime1 = geom.perp_vec((self.X - intersection)*self.__coll_factor)
                    Vprime2 = -1 * geom.perp_vec((self.X - intersection)*self.__coll_factor)
                    next_pos1 = self.X+Vprime1
                    next_pos2 = self.X+Vprime2
                    int1 = geom.find_intersection(self.X,next_pos1,wall[0],wall[1])
                    int2 = geom.find_intersection(self.X,next_pos2,wall[0],wall[1])

                    if int1 is not None and int2 is None:
                        Vprime = Vprime2
                    elif int2 is not None and int1 is None:
                        Vprime = Vprime1
                    else:
                        edge_dist1 = min(geom.euclidDistance(next_pos1,wall[0]),geom.euclidDistance(next_pos1,wall[1]))
                        edge_dist2 = min(geom.euclidDistance(next_pos2,wall[0]),geom.euclidDistance(next_pos2,wall[1]))
                        if edge_dist1 > edge_dist2:
                            Vprime = Vprime1*edge_dist1
                        else:
                            Vprime = Vprime2*edge_dist2


                    self.Vavoids.append(Vprime*9999)




######### BRAKING ##########
        #distance left to the current target
        dist_to_target = geom.euclidDistance(self.target.tolist(),self.X.tolist())
        #minimum distance this agent can come to full stop
        brake_dist = (geom.norm(self.V)**2) / (2* self.max_a)

        #is this the last target?
        if len(self.target_list) == 0:
#            print 'last target', self.target_list
            #this is the last target
            if self.__stop_at_last_target:
#                print 'gonna stop'
                #we want to stop
                if dist_to_target <= brake_dist:
#                    print 'braking in the end'
                    #we are within brake distance, stop entirely
                    Vprime = np.zeros(self.V.shape)
                    A = Vprime - self.V
                    self.A = self._constrainA(A)
                    self._doPhysics()
                    return 'stopping at last target'
                if dist_to_target <= self.__target_range:
                    #we have reached the point
#                    print 'we are done here'
                    Vprime = np.zeros(self.V.shape)
                    A = Vprime - self.V
                    self.A = np.zeros(self.A.shape)
                    self._doPhysics()
                    self.__we_are_done_here = True
            else:
                #just drift
#                print 'gonna drift'
                if dist_to_target <= self.__target_range:
#                    print 'drifting'
                    #we have reached the last target but we dont care if we stop here
                    self.A = np.zeros(self.A.shape)
                    self._doPhysics()
                    return 'drifting at last target'
        else:
#            print 'not last target'
            #this is not the last target
            if dist_to_target <= self.__target_range:
#                print 'reached'
                #we are considered to have reached the target
                #set the target to the next one
                self.target = self.target_list[0] #this is a queue
                self.target_list = self.target_list[1:] #remove the first element of the list
            if dist_to_target <= brake_dist:
#                    print 'braking in the middle'
                    #we are within brake distance, stop entirely
                    Vprime = np.zeros(self.V.shape)
                    A = Vprime - self.V
                    self.A = self._constrainA(A)
                    self._doPhysics()
                    return 'braking in the middle'



######### TOWARDS TARGET #########
        dX = self.V * self.dt * self.__braking_factor #position in breaking_factor time steps if V is unchanged
        Vprime = self.target - (self.X + dX) #desired velocity to reach the target from dX
        self.Vtarget.append(Vprime)

        self._doPhysics()
        if self.__we_are_done_here: return 'done'

        return 'cont'

    def _doCompatibilty(self):
        self.position = self.X

    def _doPhysics(self):
        #sum up all the ideal velocities
        sumV = np.zeros(np.array([0,0]).shape)
        Vprimes = self.Vavoids + self.Vtarget + self.Vbrakes
        for Vp in Vprimes:
            sumV = sumV + Vp

        #set the accel
        self.A = self._constrainV(sumV) - self.V
        self.A = self._constrainA(self.A)
        #do physics
        self.V += self.A * self.dt
        self.V = self._constrainV(self.V)
        self.X += self.V * self.dt


        #advance timer
        self.sim_time+=self.dt
        #update other stuff
        self.view.setPosition(self.X.tolist())
#        self.view.syncSignal(self.sim_time)

        self._doCompatibilty()

    def addTarget(self,T,multi=False):
        if multi:
            arraylist = [np.array(ti) for ti in T]
            self.target_list.extend(arraylist)
        else:
            self.target_list.append(np.array(T))

    def setTarget(self,T):
        self.target = np.array(T)

    def addAvoid(self,other):
        self.avoid_list.append(other)

    def addWall(self,wall,multi=False):
        if multi:
            arraylist = [w for w in wall]
            self.wall_list.extend(arraylist)
        else:
            self.wall_list.append(wall)

    def _constrainA(self,A):
        if geom.norm(A) > self.max_a:
            A = geom.normalize(A) * self.max_a
        return A

    def _constrainV(self,V):
        if geom.norm(V) > self.max_v:
            V = geom.normalize(V) * self.max_v
        return V

    def followWaypoints(self,points):
        self.addTarget(points,multi=True)
        for time in range(10000):
            res = self.tick()
#            plt.scatter(self.X[0],self.X[1],alpha = 0.2)
            if res == 'done': break
            if time%30 == 0: plt.pause(0.0001)
        return time

    def addVehiclesRange(self,veh_list):
        for v in veh_list:
            if not self.veh_in_range.has_key(v.ID):
                self.veh_in_range[v.ID]=v


if __name__ == '__main__':
    plt.axis('equal')
    models = []

#    walls = [[[30.,-20.],[30.,20.]],[[20.,-20.],[10.,20.]],[[10.,20.],[-4.,22.]]]#,[[-4.,22.],[-18.,10.]]]
#    walls = [[[30.,-20.],[30.,20.]],[[20.,-20.],[10.,20.]],[[10.,20.],[-4.,22.]],[[-4.,22.],[-18.,10.]],[[-18.,10.],[-18.,0.]]]
#    walls = [[[30.,-20.],[30.,20.]],[[20.,-20.],[10.,20.]],[[10.,20.],[-4.,22.]],[[12.,4.],[-7.,2.5]]]
    walls = [[[12.,24.],[-17.,22.5]]]
#    walls = [[[-0.3,3.42],[12.,18.]],[[-0.3,3.42],[-16.,15.]]]
    for wall in walls: geom.plot_line(wall)

    s1 = [0.,0.]
    g1 = [60.,0.]
    wps = [[10.,10.],[30.,0.],[15.,15.]]
#    plt.scatter(g1[0],g1[1])
    m1 = DynamicPoint(views.TracingView(init_pos = s1, cont=False),ID=-1,X=s1)
    t = [-2.,23.]
    plt.scatter(t[0],t[1], marker = 'x')
    m1.addTarget(t)
#    m1.addTarget(g1)
    m1.addWall(walls,multi=True)
    models.append(m1)
#    m1.followWaypoints(wps)

#    s2 = [31.,21.]
#    m2 = DynamicPoint(views.TracingView(init_pos = s2, cont=True),ID=0,X=s2)
#    models.append(m2)
#    m1.addAvoid(m2)
#
#    s3 = [48.,14.]
#    m3 = DynamicPoint(views.TracingView(init_pos = s3, cont=True),ID=1,X=s3)
#    models.append(m3)
#    m1.addAvoid(m3)
#
#    s4 = [12.,7.]
#    m4 = DynamicPoint(views.TracingView(init_pos = s4, cont=True),ID=2,X=s4)
#    models.append(m4)
#    m1.addAvoid(m4)



    for model in models:
            r = model.size
            c1 = plt.Circle(model.X,r, fill = False)
            fig = plt.gcf()
            fig.gca().add_artist(c1)

    for time in range(15000):
#        plt.clf()
#        plt.axis('equal')
#        plt.scatter(g1[0],g1[1])
#        plt.scatter(g2[0],g2[1])
        for model in models:
            model.tick()
#            r = model.size
#            c1 = plt.Circle(model.X,r, fill = False)
#            fig = plt.gcf()
#            fig.gca().add_artist(c1)
#            plt.plot([model.X[0],model.X[0]+model.A[0]],[model.X[1],model.X[1]+model.A[1]], color = 'b', alpha = 0.1)
#            plt.plot([model.X[0],model.X[0]+model.V[0]],[model.X[1],model.X[1]+model.V[1]], color = 'g', alpha = 0.1)
        if time%20 == 0:
            plt.pause(0.0001)

