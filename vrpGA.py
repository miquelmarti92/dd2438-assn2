# -*- coding: utf-8 -*-
"""
Created on Fri Feb 26 13:40:55 2016

@author: Miquel
"""

from pyevolve import *
import matlabHandler as mh
import numpy as np
import polySearch as ps
import models as md
import geometry as geom
import matplotlib.pyplot as plt
import views as vw
import operator
import random
import dynamic_point as dp
import vrep

class VRPsolver:

    def __init__(self,s,c,x,y,edges,button,pop_size=100,num_gen=1000,mut_rate=0.1):
        """Create a new instance"""
        self.num_veh=len(s)
        self.num_cus=len(c)
        self.vehicles=dict(zip(range(self.num_veh),s))
        self.customers=dict(zip(np.array(range(self.num_cus)).tolist(),c))
        self.pop_size=pop_size
        self.num_gen=num_gen
        self.vehToCust=self.genVisVeh2Cust(x,y,edges,button)
        self.custToCust=self.genVisCust2Cust(x,y,edges,button)
        self.mut_rate=mut_rate

    def genVisVeh2Cust(self,x,y,edges,button):
        mat=[[ps.astar(self.vehicles[i],self.customers[j],x,y,edges,button) for j in range(self.num_cus)] for i in range(self.num_veh)]
        return mat

    def genVisCust2Cust(self,x,y,edges,button):
        mat=[[ps.astar(self.customers[i],self.customers[j],x,y,edges,button) for j in range(self.num_cus)] for i in range(self.num_cus)]
        return mat


    def chToSol(self,ch):
        chromosome=ch.getInternalList()
        ind={}
        for i in range(self.num_veh):
            ind[i]=chromosome.index(i)
        sorted_ind = sorted(ind.items(), key=operator.itemgetter(1))
        ch=np.roll(chromosome,-sorted_ind[0][1])
        sol=[0 for i in range(self.num_veh)]
        for i in range(self.num_veh):
            if i==self.num_veh-1:
                clist=ch.tolist()[sorted_ind[i][1]-sorted_ind[0][1]+1:self.num_veh+self.num_cus]
            else:
                clist=ch.tolist()[sorted_ind[i][1]-sorted_ind[0][1]+1:sorted_ind[i+1][1]-sorted_ind[0][1]]
            clist=np.array(clist)-self.num_veh
            sol[sorted_ind[i][0]]=clist.tolist()
            sol[sorted_ind[i][0]].reverse()

        return sol

    def solToWaypoints(self,sol):
        waypoints=[]
        for j in range(self.num_veh):
            if len(sol[j]) !=0:
                p=self.vehToCust[j][sol[j][0]]
                for k in range(len(sol[j])-1):
                    p=p+self.custToCust[sol[j][k]][sol[j][k+1]]
                waypoints.append(p)
            else:
                waypoints.append([])
        return waypoints

    def eval_func(self,chromosome,plot=False):
        sol=self.chToSol(chromosome)
        score=0
        for i in range(self.num_veh):
            if np.array(sol[i]).size !=0:
                #CHECK COMPUTED MATRIX
                p=[self.vehicles[i].tolist()]+self.vehToCust[i][sol[i][0]]
                for k in range(len(sol[i])-1):
                    p+=self.custToCust[sol[i][k]][sol[i][k+1]]
                p=np.array(p)
                path=sum([np.linalg.norm(p[j+1]-p[j]) for j in range(len(p)-1)])
                score=max(score,path)
                if plot:
                    plt.text(self.vehicles[i][0]+5,self.vehicles[i][1]-5,'P:'+str(path))
        return score

    def G1DListVRPInitializator(self, genome, **args):
       lst = [i for i in xrange(genome.getListSize())]
       random.shuffle(lst)
       genome.setInternalList(lst)

    def solveVRP(self):
        max_range=self.num_cus+self.num_veh
        genome = G1DList.G1DList(max_range)
        genome.evaluator.set(self.eval_func)
        genome.initializator.set(self.G1DListVRPInitializator)
        genome.crossover.set(Crossovers.G1DListCrossoverEdge)
        ga = GSimpleGA.GSimpleGA(genome)
        ga.setGenerations(self.num_gen)
        ga.setPopulationSize(self.pop_size)
        ga.setMutationRate(self.mut_rate)
        ga.setMinimax(Consts.minimaxType["minimize"])
        ga.selector.set(Selectors.GRouletteWheel)
        ga.evolve(freq_stats=20)

        return ga.bestIndividual()
#        for i in range(self.num_gen):
#            P1, P2   ParentsSelection(pop)
#            O1   Crossover(P1,P1)
#            O2   Mutation(O1)
#            R   SolutionOutSelection(Population)
#            Replace(O2,R)

#        customers=self.customers.values()
#        self.solution=[customers[0:2],customers[2:4],customers[4],customers[5:7],customers[7]]
#        return self.solution

if __name__ == '__main__':
    s,g,x,y,edges,button,customers = mh.generateOBJstr('final6.mat') #assn2map1 or map2
    customer=np.delete(customers,[10,11],0)
    c=customer.tolist()
    R=150
    doInVrep=True
    scale=0.01
    baseName='port_sphere'
    
    
    if doInVrep:
        cid = vw.startVrep()
        err, handle = vrep.simxGetObjectHandle(cid, baseName, vrep.simx_opmode_oneshot_wait)
        objHandle=[handle]
        for st in s.tolist():
            err, newFloorHandle = vrep.simxCopyPasteObjects(cid, [objHandle[0]] , vrep.simx_opmode_oneshot_wait)
            vrep.simxSetObjectPosition(cid, newFloorHandle[0], -1, (np.array(st)*scale).tolist(), vrep.simx_opmode_oneshot)
            objHandle.extend(newFloorHandle)

    mh.draw2Dpolygons(x,y,edges,False)
    models=[]
    for i in range(len(s)):
        if doInVrep:
            view = vw.vrep_view(cid,objHandle[i+1],scale)
        else:
            view = vw.TracingView(init_pos = geom.in3d(s[i]))    
        models.append(dp.DynamicPoint(view,ID=i,X=s[i], max_v = 100.))
        models[i].view.reinit()

    try:
        plt.scatter(customer[:,0],customer[:,1],marker = 's')
    except IndexError:
        pass

    plt.scatter(s[:,0],s[:,1],marker = 'o')

#    vrp=VRPsolver(s,customer,x,y,edges,button,pop_size=200,num_gen=1000,mut_rate=0.3)
#    bestChr=vrp.solveVRP()
    sol=vrp.chToSol(bestChr)
    sol=[[1, 10, 8, 4], [0, 11, 13], [2, 6, 7], [5, 9], [12, 3]]
    wp=vrp.solToWaypoints(sol)
    [view.drawPoints([w]) for w in customers.tolist()]
#    poly_walls = []
#    for edge in edges:
#        wall = [[x[int(edge[0])],y[int(edge[0])]],[x[int(edge[1])],y[int(edge[1])]]]
#        poly_walls.append(wall)

    waypoints=wp[:]
    for i in range(len(s)):
        if len(waypoints[i]) !=0:
            plt.plot(np.hstack([s[i][0],np.array(waypoints[i])[:,0]]),np.hstack([s[i][1],np.array(waypoints[i])[:,1]]),alpha = 0.3, color = 'r')
            models[i].addTarget(waypoints[i],True)
            customer_wps = []
            for pt in waypoints[i]:            
                for cust in customer:                
                    if cust[0] == pt[0] and cust[1] == pt[1]:
                        customer_wps.append(pt)

#            models[i].addTarget(customer_wps,True)
#            for wall in poly_walls:
#                models[i].addWall(wall)

    a=[plt.text(value[0],value[1]+5,"c"+str(key)) for (key,value) in vrp.customers.items()]
    a=[plt.text(value[0],value[1]+5,"v"+str(key)) for (key,value) in vrp.vehicles.items()]

    done=[False for m in models]
    while any(d!='done' for d in done):
        done=[m.tick() for m in models]
        #Sync all models at once
        models[0].view.syncSignal()


    print "TOTAL TIME COST" + str(max([m.sim_time for m in models]))

    if doInVrep:
        vw.stopVrep(cid)

#    val=vrp.eval_func(bestChr,True)
#    print "COMPUTED TIME VIS" +str(val)

if __name__ == '__main__2':
    s,g,x,y,edges,button,customer = mh.generateOBJstr('assn2map1.mat') #assn2map1 or map2
    mh.draw2Dpolygons(x,y,edges,False)

    try:
        plt.scatter(customer[:,0],customer[:,1],marker = 's')
    except IndexError:
        pass

    plt.scatter(s[:,0],s[:,1],marker = 'o')

    [plt.text(value[0],value[1]+5,"c"+str(key)) for (key,value) in vrp.customers.items()]
    [plt.text(value[0],value[1]+5,"v"+str(key)) for (key,value) in vrp.vehicles.items()]

    genome = G1DList.G1DList(vrp.num_cus+vrp.num_veh)
    lst = [0,5,6,1,7,2,8,3,9,10,11,4,12]
    genome.setInternalList(lst)

    vrp=VRPsolver(s,customer,x,y,edges,button)

    sol=vrp.chToSol(genome)
    wp=vrp.solToWaypoints(sol)
    waypoints=wp[:]

    for i in range(len(s)):
        if len(waypoints[i]) !=0:
            plt.plot(np.hstack([s[i][0],np.array(waypoints[i])[:,0]]),np.hstack([s[i][1],np.array(waypoints[i])[:,1]]),alpha = 0.3, color = 'r')

    vrp.eval_func(genome,True)




