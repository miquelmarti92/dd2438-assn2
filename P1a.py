# -*- coding: utf-8 -*-
"""
Created on Sun Feb 28 13:27:48 2016

@author: kkalem


P1a: Solve T1 above, with the given 4-4-2 formation, d=50,
and the 10 starting positions given by the 5 starting (*) and
5 ending (x) positions of polygObst above. The vehicle model is
 the dynamic point, with (Amax=1, Vmax=100).
"""

import matlabHandler as mh
import views as vw
import formation as form
import dynamic_point as dp
import geometry as geom

import matplotlib.pyplot as plt

#s,g,x,y,edges,button,customer = mh.generateOBJstr('assn2map1.mat') #assn2map1 or map2
s,g,x,y,edges,button,customer = mh.generateOBJstr('final5.mat')

#starting = s.tolist() + g.tolist()
starting = s.tolist()
fx = 0.
fy = 0.
for i in range(len(starting)):
    fx += starting[i][0]
    fy += starting[i][1]
fx = fx / len(starting)
fy = fy / len(starting)

view = vw.TracingView();
#f = form.formation([[0,0],[1,0],[2,0],[3,0],[0,1],[1,1],[2,1],[3,1],[1,2],[2,2]],d=50, view = view, pos = [fx-100,fy-70])
f = form.formation(g.tolist(),d=1, view = view, pos = [fx,fy])

ffx =0.
ffy = 0.
for p in f.real:
    ffx += p[0]
    ffy += p[1]
ffx = ffx/len(f.real)
ffy = ffy/len(f.real)

dx = ffx - fx
dy = ffy - fy

f.moveTo([f.pos[0]-dx,f.pos[1]-dy])

agents = []

for pos in starting:
    view = vw.TracingView(init_pos = geom.in3d(pos), cont = True)
    agent = dp.DynamicPoint(view, 0, X = pos, size = 0.)
    agent.view.reinit()
    agent.__stop_at_last_target = False
    agents.append(agent)

modelposs = [agents[k].X for k in range(len(agents))]
indices = f.compute(modelposs)
for j in range(len(agents)):
    agents[j].addTarget(f.real[indices[j][1]])


for time in range(1000):
    done = ''
    for agent in agents:
        done += agent.tick()
        if done == 'done'*len(agents):
            print 'done in ',time,' ticks'
            break
    agents[0].view.syncSignal()
#    f.tick()

#for agent in agents:
#    vw.drawPath(agent.view.pos_trace)
#    plt.scatter(agent.X[0], agent.X[1])
#f.draw()